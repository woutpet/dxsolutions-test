<?php

namespace Acme\DxsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AcmeDxsBundle:Default:index.html.twig', [
    	]);
    }
}
