<?php

namespace Acme\DxsBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use \Acme\DxsBundle\Entity\Job;

class JobController extends Controller
{
    public function IndexAction()
    {
        return $this->render('AcmeDxsBundle:Job:index.html.twig', array(
            'jobs' => $this->getDoctrine()->getRepository('AcmeDxsBundle:Job')->findAll()
        ));
    }

    public function AddAction(Request $request)
    {
        $job = new Job();
        
        $form = $this->CreateJobForm($job);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->HandleForm($form);
            return $this->redirect('/jobs/' . $job->getId());
        }

        return $this->render('AcmeDxsBundle:Job:add.html.twig', array(
            'form' => $form->createView()
        ));
    }

    protected function CreateJobForm(Job $job)
    {
        $form = $this->createFormBuilder($job)
            ->add('title', TextType::class)
            ->add('company', TextType::class)
            ->add('description', TextareaType::class)
            ->add('experience', TextareaType::class)
            ->add('region', TextType::class)
            ->add('city', TextType::class)
            ->add('type', ChoiceType::class, array(
                'choices'  => array(
                    'Full time' => 'Full time',
                    'Part time' => 'Part time',
                    'Student contract' => 'Student contract'
                )))
            ->add('save', SubmitType::class, array('label' => 'Create Job'))
            ->getForm();

        return $form;
    }

    protected function HandleForm($form)
    {
        $job = $form->getData();
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($job);
        $manager->flush();
        
    }

    public function ViewAction($id)
    {
        return $this->render('AcmeDxsBundle:Job:view.html.twig', array(
            'job' => $this->getDoctrine()->getRepository('AcmeDxsBundle:Job')->find($id)
        ));
    }

    public function EditAction(Request $request, $id)
    {
        $job = $this->getDoctrine()->getRepository('AcmeDxsBundle:Job')->find($id);
        $form = $this->CreateJobForm($job);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->HandleForm($form);
            return $this->redirect('/jobs/' . $id);
        }

        return $this->render('AcmeDxsBundle:Job:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function DeleteAction(Request $request, $id)
    {

        $job = $this->getDoctrine()->getRepository('AcmeDxsBundle:Job')->find($id);
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($job);
        $manager->flush();

        return $this->redirect('/jobs');

        // return $this->render('AcmeDxsBundle:Job:delete.html.twig', array(
        //     // ...
        // ));
    }

}
